$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 5000
    });

    $('#exampleModal').on('show.bs.modal', function (e){
      console.log('el modal se esta mostrando');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });
    $('#exampleModal').on('shown.bs.modal', function (e){
      console.log('el modal se mostro');
    });
    $('#exampleModal').on('hide.bs.modal', function (e){
      console.log('el modal se oculta');
    });
    $('#exampleModal').on('hidden.bs.modal', function (e){
      console.log('el modal se oculto');
      $('#contactoBtn').prop('disabled', false);
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
    });
});